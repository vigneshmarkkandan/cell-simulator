import React, { Component } from "react";

class Buttons extends React.Component {
  render() {
    return (
      // reset and next gen buttons
      <div className="center">
        <button
          className="btn btn-primary btn-sm m-2"
          onClick={this.props.clear}
        >
          Reset
        </button>

        <button className="btn btn-primary btn-sm" onClick={this.props.nxtGen}>
          Next Gen
        </button>
      </div>
    );
  }
}

export default Buttons;
