import React, { Component } from "react";
import Grid from "./grid";
import Buttons from "./buttons";

class Main extends React.Component {
  constructor() {
    super();
    this.rows = 10;
    this.cols = 20;

    this.state = {
      gridFull: Array(this.rows)
        .fill()
        .map(() => Array(this.cols).fill(false))
    };
  }

  arrayClone = arr => {
    return JSON.parse(JSON.stringify(arr));
  };

  selectBox = (row, col) => {
    let gridCopy = this.arrayClone(this.state.gridFull);
    gridCopy[row][col] = !gridCopy[row][col];
    this.setState({
      gridFull: gridCopy
    });
  };

  nxtGen = () => {
    let gridCopy = this.arrayClone(this.state.gridFull);
    for (let i = 0; i < this.rows; i++) {
      for (let j = 0; j < this.cols; j++) {
        if (Math.floor(Math.random() * 4) === 1) {
          gridCopy[i][j] = true;
        }
      }
    }
    this.setState({
      gridFull: gridCopy
    });
  };

  initSimulation = () => {
    clearInterval(this.intervalId);
    this.intervalId = setInterval(this.startSimulation, 100);
  };

  clear = () => {
    //reset the grid
    var grid = Array(this.rows)
      .fill()
      .map(() => Array(this.cols).fill(false));
    this.setState({
      gridFull: grid
    });
  };

  startSimulation = () => {
    //initate the gen sequence
    /* Function Scope
    A box with fewer than two live neighbours dies of under-population.
  - A box with 2 or 3 live neighbours lives on to the next generation.
  - A box with more than 3 live neighbours dies of overcrowding.
  - An empty box with exactly 3 live neighbours "comes to life".
    */
    let g = this.state.gridFull;
    let g2 = this.arrayClone(this.state.gridFull);

    for (let i = 0; i < this.rows; i++) {
      for (let j = 0; j < this.cols; j++) {
        let count = 0;
        if (i > 0) if (g[i - 1][j]) count++;
        if (i > 0 && j > 0) if (g[i - 1][j - 1]) count++;
        if (i > 0 && j < this.cols - 1) if (g[i - 1][j + 1]) count++;
        if (j < this.cols - 1) if (g[i][j + 1]) count++;
        if (j > 0) if (g[i][j - 1]) count++;
        if (i < this.rows - 1) if (g[i + 1][j]) count++;
        if (i < this.rows - 1 && j > 0) if (g[i + 1][j - 1]) count++;
        if (i < this.rows - 1 && j < this.cols - 1)
          if (g[i + 1][j + 1]) count++;
        if (g[i][j] && (count < 2 || count > 3)) g2[i][j] = false;
        if (!g[i][j] && count === 3) g2[i][j] = true;
      }
    }
    this.setState({
      gridFull: g2
    });
  };

  componentDidMount() {
    this.nxtGen();
    this.initSimulation();
    this.clear();
  }

  render() {
    return (
      <div>
        <h2>Cell Simulator</h2>
        <Buttons clear={this.clear} nxtGen={this.nxtGen} />
        <Grid
          gridFull={this.state.gridFull}
          rows={this.rows}
          cols={this.cols}
          selectBox={this.selectBox}
        />
      </div>
    );
  }
}

export default Main;
