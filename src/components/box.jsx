import React, { Component } from "react";
class Box extends React.Component {
  selectBox = () => {
    const { selectBox, row, col } = this.props;
    selectBox(row, col);
  };

  render() {
    return (
      <div
        className={this.props.boxClass}
        id={this.props.id}
        onClick={this.selectBox}
      />
    );
  }
}

export default Box;
